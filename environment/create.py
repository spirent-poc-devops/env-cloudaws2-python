# -*- coding: utf-8 -*-
"""
    component.create
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to create a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

from datetime import datetime
from common import read_config, read_resources, write_resources
from common import get_map_value, set_map_value
from common import run_process, run_process_safe

def create(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    environment_type = get_map_value(config, "environment.type")
    environment_version = get_map_value(config, "environment.version")
    environment_time = datetime.now().isoformat()
    namespace = get_map_value(config, "k8s.namespace")

    # Write to config map
    run_process("kubectl delete --ignore-not-found=true configmap environment -n " + namespace)
    run_process_safe("kubectl create configmap environment -n " + namespace \
        + " --from-literal=type=" + environment_type \
        + " --from-literal=version=" + environment_version \
        + " --from-literal=time=" + environment_time)

    # Write to resource file
    set_map_value(resources, "environment.type", environment_type)
    set_map_value(resources, "environment.version", environment_version)
    set_map_value(resources, "environment.create_time", environment_time)
    write_resources(resources_path, resources)
