#!/usr/bin/env python3

import sys
import time
import traceback
import importlib.util
from common import format_duration, format_table
from common import parse_cmdline
from common import convert_to_resource_path
from common import write_info, write_error

def main(argv):
    # Parse command line arguments
    try:
        cmdline = parse_cmdline(argv)
    except Exception as e:
        print(e, file=sys.stderr)
        print("Usage: update_env.py --config=<config path> [--resources=<resources path>] [--components=<component>,...]")
        sys.exit(2)

    # Retrieve command line parameters
    config_path = cmdline["config"]
    resource_path = cmdline["resources"]
    components = cmdline["components"]

    # Set default parameter values
    if resource_path == None:
        resource_path = convert_to_resource_path(config_path)

    if components == None:
        components = [
            "kubernetes",
            "namespace",
            "environment"
        ]
    
    try:
        total_start_time = time.time()
        total_status = "SUCCESS"
        statuses = []

        # Update components in the order they defined
        for component in components:    
            try:
                start_time = time.time()
                status = "SUCCESS"

                write_info(component, "Started updating " + component + " component.", True)

                # Execute component provisioning function
                spec = importlib.util.spec_from_file_location("update", "./" + component + "/update.py")
                func = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(func)
                func.update(config_path, resource_path)

                write_info(component, "Completed updating " + component + " component.", True)
            # Catch and log any errors
            except Exception as e:
                status = "FAIL"
                total_status = "FAIL"

                print(e, file=sys.stderr)
                traceback.print_tb(e.__traceback__)
                write_error(component, "Can't update the " + component + " component. See logs above.")
            finally:
                elapsed_time = time.time() - start_time
                duration = format_duration(elapsed_time)
                statuses.append({ "Component": component, "Duration": duration, "Status": status })

                if status == "FAIL":
                    write_error(component, component + " update " + status + " in " + duration)
                else:
                    write_info(component, component + " update " + status + " in " + duration)
        
    finally:
        elapsed_time = time.time() - total_start_time
        total_duration = format_duration(elapsed_time)

        write_info("environment", "Overall update status: " + total_status)
        print(format_table(statuses))
        write_info("environment", "Total update duration: " + total_duration)


if __name__ == '__main__':
    main(sys.argv[1:])