
variable "aws_region" {
  type        = string
  description = "AWS Region"
}

variable "aws_credentials_file" {
  type        = string
  description = "AWS Credentials File"
}

variable "aws_profile" {
  type        = string
  description = "AWS Profile"
}

variable "aws_vpc" {
  type        = string
  description = "AWS VPC"
}

variable "env_nameprefix" {
  type        = string
  description = "Envrionment name prefix"
}

variable "k8s_version" {
  type        = string
  description = "Version of kubernetes cluster"
}

variable "k8s_subnets" {
  type        = list(string)
  description = "List of aws subnet ids used for k8s cluster"
}

variable "k8s_desired_nodes" {
  type        = number
  description = "Count of desired k8s cluster nodes"
}

variable "k8s_min_nodes" {
  type        = number
  description = "Count of minimal k8s cluster nodes"
}

variable "k8s_max_nodes" {
  type        = number
  description = "Count of maximal k8s cluster nodes"
}

variable "k8s_instance_type" {
  type        = string
  description = "Type of k8s node instance"
}

data "aws_eks_cluster" "eks" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

# provider "kubernetes" {
#   host                   = data.aws_eks_cluster.eks.endpoint
#   cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
#   token                  = data.aws_eks_cluster_auth.eks.token
# }

provider "kubernetes" {
  token                  = data.aws_eks_cluster_auth.eks.token
  config_path            = "~/.kube/config"
  insecure               = true
}

provider "aws" {
  region                  = var.aws_region
  shared_credentials_file = var.aws_credentials_file
  profile                 = var.aws_profile
}


module "eks" {
  source          = "terraform-aws-modules/eks/aws"

  cluster_version = var.k8s_version
  cluster_name    = "eks-${var.env_nameprefix}"
  vpc_id          = var.aws_vpc
  subnets         = var.k8s_subnets

  node_groups = {
    eks_nodes = {
      desired_capacity = var.k8s_desired_nodes
      max_capacity     = var.k8s_max_nodes
      min_capaicty     = var.k8s_min_nodes
      instance_type    = var.k8s_instance_type
    }
  }

}

output "eks_cluster_id" {
  description = "Output EKS Cluster Id"
  value       = module.eks.cluster_id
}

output "eks_cluster_arn" {
  description = "Output eks cluster arn"
  value       = module.eks.cluster_arn
}

output "eks_cluster_endpoint" {
  description = "Output EKS cluster Endpoint"
  value       = module.eks.cluster_endpoint
}
