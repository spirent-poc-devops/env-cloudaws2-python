# -*- coding: utf-8 -*-
"""
    component.create
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to create a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import json
from common import read_config, read_resources, write_resources
from common import get_map_value, set_map_value
from common import write_error
from common import run_process, read_process
from common import save_terraform_state
from kubernetes import env_terraform, init_terraform

def create(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    environment_prefix = get_map_value(config, "environment.prefix")
    path = os.path.abspath(os.path.dirname(__file__))

    # Set terraform env variables and init terraform folder
    tf_vars = env_terraform(config, resources)
    k8s_terraform_path = init_terraform(environment_prefix)

    # Run terraform scripts
    os.chdir(k8s_terraform_path)
    # exit_code = run_process("terraform init")
    # if exit_code != 0:
    #     os.chdir(path + "/..")
    #     write_error("kubernetes", "Can't initialize terraform. Watch logs above or check " + k8s_terraform_path +" folder content.")

    # exit_code = run_process("terraform plan " + tf_vars)
    # if exit_code != 0:
    #     os.chdir(path + "/..")
    #     write_error("kubernetes", "Can't execute terraform plan. Watch logs above or check " + k8s_terraform_path +" folder content.")
    
    # exit_code = run_process("terraform apply -auto-approve " + tf_vars)
    # # if exit_code != 0:
    # #     os.chdir(path + "/..")
    # #     write_error("kubernetes", "Can't create cloud resources. Watch logs above or check " + k8s_terraform_path +" folder content.")

    # Get terraform outputs
    outputs = json.loads(str(read_process("terraform output -json")))
    eks_cluster_id = outputs["eks_cluster_id"]["value"]
    eks_cluster_arn = outputs["eks_cluster_arn"]["value"]
    eks_cluster_endpoint = outputs["eks_cluster_endpoint"]["value"]

    os.chdir(path + "/..")

    save_terraform_state("kubernetes", environment_prefix)

    set_map_value(resources, "k8s.cluster_name", eks_cluster_id)
    set_map_value(resources, "k8s.cluster_arn", eks_cluster_arn)
    set_map_value(resources, "k8s.cluster_endpoint", eks_cluster_endpoint)
    write_resources(resources_path, resources)

    # Configure access to cluster
    aws_region = get_map_value(config, "aws.region")
    run_process(f"aws eks update-kubeconfig --region {aws_region} --name {eks_cluster_id}")