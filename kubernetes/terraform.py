# -*- coding: utf-8 -*-
"""
    component.terraform
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to process terraform files
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import shutil
from common import sync_terraform_state
from common import get_map_value, set_map_value

def env_terraform(config, resources):
    k8s_subnets_str = "\\\",\\\"".join(get_map_value(config, "k8s.subnets"))
    subnets = f"\"[\\\"{k8s_subnets_str}\\\"]\""

    vars_str = "-var env_nameprefix=" + get_map_value(config, "environment.prefix") + \
        " -var aws_region=" + get_map_value(config, "aws.region") + \
        " -var aws_credentials_file=" + get_map_value(config, "aws.credentials_file") + \
        " -var aws_profile=" + get_map_value(config, "aws.profile") + \
        " -var aws_vpc=" + get_map_value(config, "aws.vpc") + \
        " -var k8s_version=" + get_map_value(config, "k8s.version") + \
        " -var k8s_desired_nodes=" + str(get_map_value(config, "k8s.desired_nodes")) + \
        " -var k8s_min_nodes=" + str(get_map_value(config, "k8s.min_nodes")) + \
        " -var k8s_max_nodes=" + str(get_map_value(config, "k8s.max_nodes")) + \
        " -var k8s_instance_type=" + get_map_value(config, "k8s.instance_type") + \
        " -var k8s_subnets=" + subnets
    
    return vars_str


def init_terraform(environment_prefix):
    path = os.path.abspath(os.path.dirname(__file__))
    k8s_terraform_path = path + "/../temp/kubernetes_" + environment_prefix

    # Create terraform folder if not exists
    if not os.path.exists(k8s_terraform_path):
        os.mkdir(k8s_terraform_path)

    # Load terraform state
    sync_terraform_state("kubernetes", environment_prefix)

    # Copy terraform scripts to terraform folder
    shutil.copy(path + "/templates/k8s.tf", k8s_terraform_path)

    return k8s_terraform_path