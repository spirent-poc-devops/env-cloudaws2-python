# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> Spirent VisionWorks Cloud AWS Environment

This is the environment automation scripts to create, update and delete production Cloud AWS Environments.

The environment consists of the following components:

* **Kubernetes** EKS cluster to run infrastructure services and application components
* **Management Station** for access control and infrastructure management

In the 2nd major update of the environment all infrastructure were moved out of the environment and placed into the [package-infraservices-helm](https://git.vwx.spirent.com/vwx-delivery/package-infraservices-helm) component

<img src="design.png" width="800">

Quick links:

* [Delivery Platform Requirements](https://frkengjira01.spirentcom.com/confluence/display/VWX/Delivery+Platform+Requirements)
* [Delivery Platform Architecture](https://frkengjira01.spirentcom.com/confluence/display/VWX/Delivery+Platform+Architecture)
* [Change Log](CHANGELOG.md)
* [Infrastructure Services](https://git.vwx.spirent.com/vwx-delivery/package-infraservices-helm)

## Use

Start the process from any computer. Download released package with environment provisioning scripts from http://artifactory.vwx.spirent.com/misc-binaries/vwx-delivery/env-cloudaws-python-2.0.0.zip

```bash
wget http://artifactory.vwx.spirent.com/misc-binaries/vwx-delivery/env-cloudaws-python-2.0.0.zip
```

Unzip scripts from the package

```bash
unzip env-cloudaws-python-2.0.0.zip
```

Go to the `config` folder and create configuration for a new environment.
Read configuration section below, copy [default_config.json](config/default_config.json) and set required values.

Your configuration may look like the sample below.

```json
{
  "environment": {
    "type": "cloudaws",
    "version": "2.0.0"
  }
}
```

Install prerequisites on your computer. The system should have terraform 14 or higher

Logging into AWS account through aws roles
1. User should be logged to the appropriate account role through `aws-azure-login`
2. Once the user is authenticated provide export the AWS Profile
```bash
export AWS_PROFILE=<profile_name>
```
Note: Profile name should be found at `~/.aws/config`

Create a management station, that will be used to create new environments.

```bash
python create_mgmt_station.py --config=<path to config file>
```

After you run the script you shall see a resource file (ending with `resources.json`) next to the config file you created.

Connect to the newly created management station (see it's address and credentials in the script output).
Install the environment scripts following the steps above and copy into `config` folder on the management station
config and resource files from your local computer. **The following steps shall be performed from the management station**.

Create a new environment. The resources file will contain references to newly created resources.

```bash
python create_env.py --config=<path to config file>
```

When environment configuration changes, you can update it without loosing data or reinstalling applications.

```bash
python update_env.py --config=<path to config file>
```

Delete the environment when its no longer needed to free up used computing resources.

```bash
python delete_env.py --config=<path to config file>
```

## Configuration

The environment configuration supports the following configuration parameters:

Parameter | Type| Default | Description
--------- | --- | ------- | ---------
env.type | string | cloudaws | Type of the created environment

Configuration parameters for the sample component:
|Parameter|Type|Default|Description|
|--- |--- |--- |--- |
|component.message_1|string|Default message 1|A message for the first output|
|component.message_2|string|Default message 2|A message for the second output|
<!-- Todo -->

Example environment config file:
```json
{
    "environment": {
        "type"                          : "cloudaws",
        "prefix"                        : "testdemo",
        "version"                       : "1.0.0"
    },
    "aws": {
        "account_id"                    : "072434860318",
        "vpc"                           : "vpc-09e59cb6c44b1c5c4",
        "public_subnets"                : ["subnet-028bb6b6d2ca7342e", "subnet-0c637d6329c805b3b", "subnet-0c331448bb9cb9912"],
        "private_subnets"               : ["subnet-0cc94ff526fab52b4","subnet-0a943b8f773a8b127","subnet-08e5e953271c10fe3"],
        "region"                        : "us-east-1", 
        "namespace"                     : "sbx006",
        "stage"                         : "sandbox",
        "zone_id"                       : "Z0880600MVJB229J5CWW",
        "subdomain"                     : ".sbx006.spirentcds.io",
        "vpc_default_sg"                : "sg-0ad956e2f350f92e7"
    },
    "mgmtstation": {
        "instance_ami"                  : "ami-08cc10a3eebe46341",
        "instance_type"                 : "t3.medium",
        "root_volume_size"              : "50",
        "instance_username"             : "ubuntu",
        "copy_project_to_mgmt_station"  : true
    },
    "eks": {
        "version"                       : "1.19",
        "worker_instance_type"          : "t3.medium",
        "workers_max_size"              : "3",
        "workers_min_size"              : "2",
        "worker_cpu_utilization_high_threshold_percent" : "80",
        "worker_cpu_utilization_low_threshold_percent"  :"5",
        "namespace": "infra"
    }
}
```

## Resources

As the result of the scripts information about created resources will be saved into a resource file that is ended with `resources.json`
and placed next to the configuration file used to create an environment.

Resource parameters saved for the sample component:
|Parameter|Type|Description|
|--- |--- |--- |
|component.output_1|string|The first output|
|component.output_2|string|The second output|
<!-- Todo -->

Example environment resources file:
```json
{
  "environment": {
    "version": "1.0.0",
    "type": "cloudaws",
    "create_time": "2021-03-24T18:30:21.6677552+00:00"
  },
  "eks": {
    "worker_instance_type": "t3.medium",
    "worker_cpu_utilization_low_threshold_percent": "5",
    "cluster_name": "sbx006-sandbox-testdemo-cluster",
    "worker_autoscaling_min_size": "2",
    "workers_autoscaling_group_name": "sbx006-sandbox-testdemo-20210323032814881700000009",
    "cluster_arn": "arn:aws:eks:us-east-1:072434860318:cluster/sbx006-sandbox-testdemo-cluster",
    "worker_autoscaling_max_size": "3",
    "workers_security_group_name": "sbx006-sandbox-testdemo-workers",
    "workers_security_group_arn": "arn:aws:ec2:us-east-1:072434860318:security-group/sg-0708ef66ac96b621d",
    "workers_security_group_id": "sg-0708ef66ac96b621d",
    "workers_role_arn": "arn:aws:iam::072434860318:role/sbx006-sandbox-testdemo-workers",
    "worker_cpu_utilization_high_threshold_percent": "80",
    "cluster_role_arn": "arn:aws:iam::072434860318:role/sbx006-sandbox-testdemo-cluster",
    "cluster_endpoint": "https://EF7169C43454CDB092A57C17B22ABF4D.gr7.us-east-1.eks.amazonaws.com",
    "workers_autoscaling_group_id": "sbx006-sandbox-testdemo-20210323032814881700000009",
    "version": "1.19",
    "worker_autoscaling_group_arn": "arn:aws:autoscaling:us-east-1:072434860318:autoScalingGroup:13818ace-b1ab-47b1-89f1-6818858e8bee:autoScalingGroupName/sbx006-sandbox-testdemo-20210323032814881700000009",
    "workers_name": "sbx006-sandbox-testdemo-workers"
  },
  "mgmtstation": {
    "keypair_name": "sbx006-sandbox-demotest-mgmt-station",
    "instance_name": "sbx006-sandbox-demotest-mgmt-station",
    "public_ip": "3.80.41.239",
    "envprefix": "demotest"
  }
}
```

## Contacts

This environment was created and currently maintained by the team managed by *Sergey Seroukhov* and *Raminder Singh*.
