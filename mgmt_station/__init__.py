# -*- coding: utf-8 -*-
"""
    component.__init__
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Component module definition
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

__all__ = [
    'env_terraform', 'init_terraform', 
    'create', 
    'delete'
    ]

from .terraform import env_terraform, init_terraform
from .create import create
from .delete import delete