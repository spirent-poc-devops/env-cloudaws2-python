# -*- coding: utf-8 -*-
"""
    common.convert
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Formatting functions
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import math

def format_duration(seconds):
    return "" + str(math.trunc(seconds / 3600)).rjust(2, '0') \
        + ":" + str(math.trunc((seconds % 3600) / 60)).rjust(2,'0') \
        + ":" + str(math.trunc(seconds % 60)).rjust(2,'0')


def format_table(values):
    if not isinstance(values, list):
        values = [values]

    # Define colums and their width
    columns = dict()
    for value in values:
        for key in value:
            width = len(str(value[key]))
            if not key in columns:
                columns[key] = max(len(key), width)
            else:
                columns[key] = max(width, columns[key])

    result = "\n"

    # Print table headers
    line = ""
    for column in columns:
        width = columns[column]
        line += column.ljust(width, " ") + "  "
    result += line + "\n"

    # Print table delimiters
    line = ""
    for column in columns:
        width = columns[column]
        line += "".ljust(width, "-") + "  "
    result += line + "\n"

    # Print table cells
    for value in values:
        line = ""
        for column in columns:
            cell = value[column] if column in value else ""
            width = columns[column]
            line += cell.ljust(width, " ") + "  "
        result += line + "\n"

    return result
