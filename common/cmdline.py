# -*- coding: utf-8 -*-
"""
    common.cmdline
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Functions to parse commandline arguments
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import getopt

def parse_cmdline(argv):
    opts, args = getopt.getopt(argv, "c:r:i:", ["config=", "resources=", "components="])

    config_path = None
    resource_path = None
    components = None
    for opt, arg in opts:
        if opt in ('-c', '--config'):
            config_path = arg
        elif opt in ('-r', '--resources'):
            resource_path = arg
        elif opt in ('-i', '--components'):
            components = arg.split(",") if arg != None and arg != '' else []

    if config_path == None:
        raise Exception("Config parameter is not set")

    result = { "config": config_path, "resources": resource_path, "components": components }
    return result